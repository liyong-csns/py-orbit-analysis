import os
import json
import argparse
import math
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm
from scipy.optimize import root,fsolve
from lib.read_RF_curve import *
from lib.read_dual_RF_curve import *

##################################################################
# ToolPack can get the whole bunch infomation from .json file
# Please ensure the folder 'py-orbit-analysis' at the same level as 'csns_lattice' 
# Run : source setPath.sh
#       python3 ToolPack.py <your config file name in 'csns_lattice'>.json
# You can find the output information in ./picReposi/<your Dump File name>
# Author: liuhy@ihep.ac.cn
################################################################## 

class ToolPack:
    """Using for analysing PyORBIT output files"""
    def __init__(self, config):
        cfg = json.load(config)
        self.name = cfg['config']['name']
        self.machine = cfg['machine']
        self.rf = cfg['rfcavity']
        #some parameters for CSNS 
        self.latticLenth = 227.92
        self.pi = 3.141592653589793
        self.speed_of_light = 299792458
        self.harmonicNum = 2
        self.ZtoPhi = -2.0*self.harmonicNum*self.pi/self.latticLenth
        self.diag = cfg['diagnostic']
        self.dump = self.diag['dump']
        self.injection = cfg['injection']
        self.track = cfg['track']
        self.picReposi = './picReposi/' + self.name + "/"
        if os.path.exists(self.picReposi) == True:
            print("Warning! You already have a folder at " + self.picReposi)
        else:
            os.makedirs(self.picReposi)    
        self.__read_RF_File()  


        ##################################################################
        # __read_RF_File(self)
        # private method :get rf settings from config file
        ################################################################## 
    def __read_RF_File(self):
        if not self.rf["enable"]:
            return
        model = 'AC'
        if model == 'AC':
            curve = self.rf["curve"]
            if curve.split(".")[-1] == 'multi' :
                [self.RFtime,self.values,RFV,RFphase,self.harms] = read_RF_curve(curve)
                self.RFV = RFV
                self.RFphase = RFphase
            elif curve.split(".")[-1] == 'dual' :
                [self.RFtime,self.RFV,self.RFphase,self.RF2VRatio,self.RF2Phase,self.values] = read_dual_RF_curve(curve)

        else:
            self.dESync = self.rf['desync']
            self.RFHNum = self.rf['num']
            self.RFVoltage = self.rf['voltage']
            self.RFPhase = self.rf['phase']



        ##################################################################
        #__read_Particl_Info(self, turn, pos = None , SpaceReturn = "xyz")
        # private method : read particle 6D coordinate in () turn at () pos
        # the last option SpaceReturn :"xyz" means  return x,xp,y,yp,z,dE
        #                               "z"  means  return z only
        #                              "zdE" means  return z,dE
        ################################################################## 
    def __read_Particl_Info(self, turn, pos = None , SpaceReturn = "xyz"):
        Dump_Name = self.dump['name'].split(".")[0]
        Dump_Suffix = "bunch"
        bunchFileName = str(turn)+"_"+ Dump_Name+"_"+str(pos)
        BunchFile = open(self.dump['folder']+'/'+bunchFileName+"."+ Dump_Suffix,'r')
        
        tempData = []
        x = []
        xp = []
        y = []
        yp = []
        z = []
        dE =[]
        while True:
            templine = BunchFile.readline()
            if templine:
                if templine[0] == "%":
                    continue
                else: 
                    tempData = templine.split(" ")
            else:
                break
            if math.isnan(float(tempData[4])) or math.isnan(float(tempData[5])):
                print("Remove one line of invalid data from " + bunchFileName)
                continue
            x.append(float(tempData[0]))
            xp.append(float(tempData[1]))
            y.append(float(tempData[2]))
            yp.append(float(tempData[3]))
            z.append(float(tempData[4]))
            dE.append(float(tempData[5]))
            tempData = []
        BunchFile.close()
        if SpaceReturn == "xyz" :
            return x,xp,y,yp,z,dE
        elif SpaceReturn == "z":
            return z
        elif SpaceReturn == "zdE" :
            return z,dE


        ##################################################################
        #__read_PartTune_Info(self, turn, pos = None )
        # private method : read particle Betatron Tune and Action
        ################################################################## 
    def __read_PartTune_Info(self, turn, pos = None ):
        Dump_Name = self.dump['name'].split(".")[0]
        Dump_Suffix = "bunch"
        bunchFileName = str(turn)+"_"+ Dump_Name+"_"+str(pos)
        BunchFile = open(self.dump['folder']+'/'+bunchFileName+"."+ Dump_Suffix,'r')
        tempData = []
        xTune = []
        yTune = []
        xAction = []
        yAction = []
        while True:
            templine = BunchFile.readline()
            if templine:
                if templine[0] == "%":
                    continue
                else: 
                    tempData = templine.split(" ")
            else:
                break
            if math.isnan(float(tempData[4])) or math.isnan(float(tempData[5])):
                print("Remove one line of invalid data from " + bunchFileName)
                continue
            xTune.append(4+float(tempData[8]))
            yTune.append(4+float(tempData[9]))
            xAction.append(float(tempData[10]))
            yAction.append(float(tempData[11]))
            tempData = []
        BunchFile.close()
        return xTune,yTune,xAction,yAction


        #######################################################################
        # __read_Bunch_Info(self, turn, pos = None)
        # private method : Read Bunch Information in () turn at () pos from 
        #                  header in your bunch file.
        #                  Return a dictionary of Bunch information 
        #                  Use getBunchKeyWords to see the keywords in BunchDic 
        ####################################################################### 
    def __read_Bunch_Info(self, turn, pos = None):
        BunchDic = {}
        Dump_Name = self.dump['name'].split(".")[0]
        Dump_Suffix = "bunch"
        bunchFileName = str(turn)+"_"+ Dump_Name+"_"+str(pos)
        BunchFile = open(self.dump['folder']+'/'+bunchFileName+"."+ Dump_Suffix,'r')
        while True:
            templine = BunchFile.readline()
            if templine:
                if templine[0] == "%":
                    templist = templine.split(" ")
                    #print(templist)
                    if templist[1] =="BUNCH_ATTRIBUTE_DOUBLE":
                        BunchDic[templist[2]] = templist[-2]
                    elif templist[2].split("_")[0] == "SYNC":       #get syncPart info 
                        BunchDic[templist[2]] = []
                        BunchDic[templist[2]].append(templist[3])   #x info
                        BunchDic[templist[2]].append(templist[4])   #y info                     
                        BunchDic[templist[2]].append(templist[5])   #z info
                    elif templist[2].split("_")[0] == "info":
                        BunchDic[templist[4]] = templist[-2]   #beta gama energy info 

                else: 
                    break
            else:
                break
        BunchDic["SYNC_PART_TIME"] = float(BunchDic["SYNC_PART_TIME"][0])  # i can't figure out a better way 
        return BunchDic

        ##################################################################
        # getBunchKeyWords(self)
        # method : If you want to know the keywords in BunchDic, 
        #          this function can return a list of BunchDic keywords
        ################################################################## 
    def getBunchKeyWords(self):
        turn = self.dump['start']
        pos = self.dump['positions'][0]
        BunchDic = self.__read_Bunch_Info(turn, pos)
        return BunchDic.keys()


        ##################################################################
        # plotPhaseSpace(self, turn, pos = None,phaseSpace = "xyz")
        # method : plot the PhaseSpace of "xyz" at () pos in () turn
        #          some parameters are not open now.
        ##################################################################
    def plotPhaseSpace(self, turn, pos = None,phaseSpace = "xyz"):
        (x,xp,y,yp,z,dE) = self.__read_Particl_Info(turn, pos)
        if "z" in phaseSpace :
            outputPath = self.picReposi +  "ZphaseSpace/" 
            if os.path.exists(outputPath) != True:
                os.makedirs(outputPath)
            #################################################
            #
            #You can change the following parameters to make
            # the picturn you like.
            #
            #################################################
            plt.figure()
            plt.title(str(turn)+'  turn',fontsize='large')
            plt.hist2d(z, dE, bins = 256, norm = LogNorm())
            plt.xlabel('z')
            plt.ylabel('dE')
            plt.savefig(outputPath+str(turn)+"_Zphasespace.png")
            plt.close()

            plt.figure()
            plt.title(str(turn)+'  turn',fontsize='large')
            plt.hist(z,bins = 256)
            plt.xlabel('z')
            plt.savefig(outputPath+str(turn)+"_ZDistribution.png")
            plt.close()
        else :
            pass
        if "x" in phaseSpace:
            outputPath = self.picReposi +  "XphaseSpace/" 
            if os.path.exists(outputPath) != True:
                os.makedirs(outputPath)
            plt.figure()
            plt.title(str(turn)+'  turn',fontsize='large')
            plt.hist2d(x,xp,bins = 256 ,norm = LogNorm())
            plt.xlabel('x')
            plt.ylabel('xp')
            plt.savefig(outputPath+str(turn)+"_Xphasespace.png")
            plt.close()

            plt.figure()
            plt.title(str(turn)+'  turn',fontsize='large')
            plt.hist(x,bins = 256)
            plt.xlabel('x')
            plt.savefig(outputPath+str(turn)+"_XDistribution.png")
            plt.close()
        else:
            pass
        if "y" in phaseSpace:
            outputPath = self.picReposi +  "YphaseSpace/" 
            if os.path.exists(outputPath) != True:
                os.makedirs(outputPath)
            plt.figure()
            plt.title(str(turn)+'  turn',fontsize='large')
            plt.hist2d(y,yp,bins = 256, norm = LogNorm())
            plt.xlabel('y')
            plt.ylabel('yp')
            plt.savefig(outputPath+str(turn)+"_Yphasespace.png")
            plt.close()

            plt.figure()
            plt.title(str(turn)+'  turn',fontsize='large')
            plt.hist(y,bins = 256)
            plt.xlabel('y')
            plt.savefig(outputPath+str(turn)+"_YDistribution.png")
            plt.close()
        else:
        	pass

        if "x" in phaseSpace and "y" in phaseSpace:
            outputPath = self.picReposi +  "XYTransverse/"
            if os.path.exists(outputPath) != True:
                os.makedirs(outputPath)
            plt.figure()
            plt.title(str(turn)+'  turn',fontsize='large')
            plt.hist2d(x,y,bins = 256, norm = LogNorm())
            plt.xlabel('x')
            plt.ylabel('y')
            plt.savefig(outputPath+str(turn)+"_XYTrans.png")
            plt.close()
        else:
            pass
        
    def plotTune(self, turn, pos = None):
        (xTune,yTune,xAction,yAction) = self.__read_PartTune_Info(turn, pos)
        outputPath = self.picReposi +  "XYtune/" 
        if os.path.exists(outputPath) != True:
            os.makedirs(outputPath)
            #################################################
            #
            #You can change the following parameters to make
            # the picturn you like.
            #
            #################################################
        plt.figure()
        plt.title(str(turn)+'  turn',fontsize='large')

        plt.hist2d(xTune, yTune, bins = 256, norm = LogNorm(), range = [[4,5],[4,5]])
        func = lambda x,pos: "{:g}".format(x*1000)
        plt.plot([4,5],[4.5,4.5],color = 'blue')
        plt.plot([4.87,4.87],[4,5],color = "red")
        plt.plot([4.85,4.85],[4,5],color = "red")
        plt.plot([4,5],[4.81,4.81],color = "red")
        plt.plot([4,5],[4.79,4.79],color = "red")
        plt.plot([4.5,4.5],[4,5],color = 'blue')
        plt.plot([4,5],[4,5],color = 'blue')

        plt.xlabel('xTune')
        plt.ylabel('yTune')
        plt.savefig(outputPath+str(turn)+"_TuneFootprint.png")
        plt.close()

        outputPath = self.picReposi +  "ActionPrint/" 
        if os.path.exists(outputPath) != True:
            os.makedirs(outputPath)
        plt.figure()
        plt.title(str(turn)+'  turn',fontsize='large')
        plt.hist2d(xAction,yAction,bins = 256 ,norm = LogNorm())
        plt.xlabel('Jx')
        plt.ylabel('Jy')
        plt.savefig(outputPath+str(turn)+"_Action.png")
        plt.close()


        #######################################################################
        # bf_cal(self, turn ,pos = None ,nBins = 1024)
        # method : Calculate buching factor in ()turn at ()pos.
        #          Algorithm is base on ORBIT  
        ####################################################################### 
    def bf_cal(self, turn ,pos = None ,nBins = 1024):
        deltaPhiBin = 2*self.pi/(nBins)
        z = self.__read_Particl_Info(turn, pos, "z")
        phiCount = []
        for i in range(nBins):
            phiCount.append(0)
        for i in range(len(z)):
            w = (z[i]*self.ZtoPhi + self.pi) / deltaPhiBin;
            iLocal = round(w);
            position = w - iLocal;
            iLocal += 1;
            if iLocal < 0:
                iLocal = nBins - 1
            if iLocal > nBins - 1:
                iLocal = 0
            im = iLocal - 1;
            if im < 0:
                im = nBins - 1
            ip = iLocal + 1
            if ip > nBins - 1:
                ip = 0
            phiCount[im] += 0.5 * (0.5 - position) * (0.5 - position)
            phiCount[iLocal] += 0.75 - position * position
            phiCount[ip] += 0.5 * (0.5 + position) * (0.5 + position)
            buch_factor = sum(phiCount)/(max(phiCount)*nBins)
        return  buch_factor


        ###########################################################################################
        # pf_cal(self, turn ,pos = None)
        # method : Calculate momentum filling factor in ()turn at ()pos.
        #          In Single Harmonic situation: Pf = BunchHeigh/BucketHeigh
        #          In Dual Harmonic situation: Pf = BunchPotentialHeighDiffer/BucketPotentialHeighDiffer
        #          They'll come to the same result theoretically
        ############################################################################################
    def pf_cal(self, turn ,pos = None):
        BunchDic = self.__read_Bunch_Info(turn, pos)
        (z,dE) = self.__read_Particl_Info(turn, pos, "zdE")
        RFtime = self.RFtime
        M0 = float(BunchDic['mass'])
        gama = float(BunchDic['gamma=1/sqrt(1-(v/c)**2)'])
        beta = float(BunchDic['beta=v/c'])
        UpdE = max(dE)
        phib2 = z[dE.index(UpdE)]*self.ZtoPhi
        phis = float(BunchDic['SYNC_PART_COORDS'][-1])*self.ZtoPhi
        TimeP = float(BunchDic['SYNC_PART_TIME'])
        GamaTrans = 4.89
        Yita = abs(1/GamaTrans/GamaTrans - 1/gama/gama)
        h = 2
        E = gama * M0
        curve = self.rf['curve']
        n_tuple = len(RFtime) - 1
        RFV = interp(TimeP, n_tuple, RFtime, self.RFV)
        phaseAdd = interp(TimeP, n_tuple, RFtime, self.RFphase)/180*self.pi
        phis += phaseAdd
        if curve.split(".")[-1] == 'multi' :
            BuketdE = 2*math.sqrt(beta*beta*E*RFV/2/math.pi/h/Yita)*math.sqrt(math.cos(phis)-(math.pi-2*phis)/2*math.sin(phis))
            Pf = UpdE / BuketdE
        elif curve.split(".")[-1] == 'dual' :
            phi2 =  interp(TimeP, n_tuple, RFtime, self.RF2Phase)*self.pi/180
            a2 = interp(TimeP, n_tuple, RFtime, self.RF2VRatio)
            phibk2 = solveTheFunction(phis,a2,phi2) 
            phi = phib2   
            phib2 = max(z)*self.ZtoPhi
            Wb = math.cos(phi) - math.cos(phib2) + (phi - phib2)*math.sin(phis)\
               - a2/2*(math.cos(2*(phi-phis)+phi2)-math.cos(2*(phib2-phis)+phi2)+2*(phi - phib2)*math.sin(phi2))
            
            Wbk =math.cos(phi) - math.cos(phibk2) + (phi - phibk2)*math.sin(phis)\
               - a2/2*(math.cos(2*(phi-phis)+phi2)-math.cos(2*(phibk2-phis)+phi2)+2*(phi - phibk2)*math.sin(phi2))

            Pf = math.sqrt(abs(Wb/Wbk))   
        return Pf


    ##########################################################
    # potentialPlot(self, turn, pos = None)
    # method : Plot the potential function of dual-harmonic RF  
    ##########################################################
    def potentialPlot(self, turn, pos = None):
        outputPath = self.picReposi +  "potentialPlot/" 
        if os.path.exists(outputPath) != True:
            os.makedirs(outputPath)
        BunchDic = self.__read_Bunch_Info(turn, pos)
        RFtime = self.RFtime
        TimeP = float(BunchDic['SYNC_PART_TIME'])
        phis = float(BunchDic['SYNC_PART_COORDS'][-1])*self.ZtoPhi
        gama = float(BunchDic['gamma=1/sqrt(1-(v/c)**2)'])
        beta = float(BunchDic['beta=v/c'])
        n_tuple = len(RFtime) - 1
        GamaTrans = 4.89
        Yita = abs(1/GamaTrans/GamaTrans - 1/gama/gama)
        RFV = interp(TimeP, n_tuple, RFtime, self.RFV)
        phi2 =  interp(TimeP, n_tuple, RFtime, self.RF2Phase)*self.pi/180
        r =  interp(TimeP, n_tuple, RFtime, self.RF2VRatio)
        phi = np.linspace(-2*self.pi,2*self.pi,50)
        V = np.cos(phis) - np.cos(phi) + (phis-phi)*np.sin(phis) - r/2*(np.cos(phi2)-np.cos(phi2+2*(phi-phis))-2*(phi-phis)*np.sin(phi2))
        
        plt.figure()
        plt.plot(phi,V)
        plt.xlabel('phi')
        plt.ylabel('V (potential)')
        plt.savefig(outputPath+str(turn)+"_PotentialField.png")
        plt.close()


    ##########################################################
    # plotPhiDeltP(self, turn, pos = None):
    # method : Plot longitudinal phase space as (phi,P)
    #          The P means =  (-h*yita/mius)*deltap/p
    ##########################################################
    def plotPhiDeltP(self, turn, pos = None):
        (z,dE) = self.__read_Particl_Info(turn, pos, "zdE")
        BunchDic = self.__read_Bunch_Info(turn, pos)
        M0 = float(BunchDic['mass'])
        gama = float(BunchDic['gamma=1/sqrt(1-(v/c)**2)'])
        beta = float(BunchDic['beta=v/c'])
        RFtime = self.RFtime
        TimeP = float(BunchDic['SYNC_PART_TIME'])
        n_tuple = len(RFtime) - 1
        RFV = interp(TimeP, n_tuple, RFtime, self.RFV)
        phi2 =  interp(TimeP, n_tuple, RFtime, self.RF2Phase)*self.pi/180
        a2 = interp(TimeP, n_tuple, RFtime, self.RF2VRatio)
        phis = float(BunchDic['SYNC_PART_COORDS'][-1])*self.ZtoPhi
        h = 2
        GamaTrans = 4.89
        Yita = abs(1/GamaTrans/GamaTrans - 1/gama/gama)
        P = []
        phi = []
        mius = math.sqrt(h*RFV*Yita/(2*math.pi*beta*beta*gama*M0))
        for i in range(len(z)):
            phi.append(z[i]*self.ZtoPhi)
            P.append(-dE[i]/(gama*M0*beta*beta)*h*Yita/mius)
        phibk2 = solveTheFunction(phis,a2,phi2)
        phibk = np.linspace(-math.pi,math.pi,128)
        V = np.cos(phibk2) - np.cos(phibk) + (phibk2-phibk)*np.sin(phis) - a2/2*(np.cos(phi2+2*(phibk2-phis))-np.cos(phi2+2*(phibk-phis))-2*(phibk-phibk2)*np.sin(phi2))
        Pup = np.sqrt(2*np.fabs(V))
        Pdown = - Pup
        outputPath = self.picReposi +  "CanonicalLongi/" 
        if os.path.exists(outputPath) != True:
            os.makedirs(outputPath)
        plt.figure()
        plt.title(str(turn)+'  turn',fontsize='large')
        plt.plot(phibk,Pup)
        plt.plot(phibk,Pdown)
        plt.hist2d(phi,P,bins = 256 ,norm = LogNorm())
        my_x_ticks = np.arange(-3, 4, 0.5)
        my_y_ticks = np.arange(-2, 2.5, 0.5)
        plt.xticks(my_x_ticks)
        plt.yticks(my_y_ticks)
        plt.xlabel('phi (rad)')
        plt.ylabel('P (Canonical)')
        plt.savefig(outputPath+str(turn)+"_CanonicalLongiPhaseSpace.png")
        plt.close()
    ##########################################################
    # calLonEmittance(self, turn, pos = None)
    # method : Using the integration method to calculate 
    #          Longitudinal Emittance
    ########################################################## 

    def calLonEmittance(self, turn, pos = None, nBins = 1024):
        (z,dE) = self.__read_Particl_Info(turn, pos, SpaceReturn = 'zdE')
        BunchDic = self.__read_Bunch_Info(turn, pos)
        beta = float(BunchDic['beta=v/c'])
        minz = min(z)
        zPerBin = (max(z) - minz)/nBins
        dEBins = []
        for i in range(nBins+1):
            dEBins.append([]) 
        for i in range(len(z)):
            index = round((z[i] - minz)/zPerBin)
            dEBins[index].append(dE[i])
        longEmittance = 0
        for i in range(nBins):
            longEmittance += calBinArea(dEBins, i, nBins)*zPerBin/(self.speed_of_light*beta)
        return longEmittance*1e9
    


    def calAdiabaticCoefficient(self, turn, pos = None):
        TIMEP1,TS1 = self.calSynctronTune(turn,pos)
        TIMEP2,TS2 = self.calSynctronTune(turn+self.dump['interval'],pos)
        AdiabaticCoe = 1/math.pi/2*abs((TS1-TS2)/(TIMEP2-TIMEP1))
        return AdiabaticCoe        


    def calSynctronTune(self, turn, pos = None):
        BunchDic = self.__read_Bunch_Info(turn, pos)
        M0 = float(BunchDic['mass'])
        gama = float(BunchDic['gamma=1/sqrt(1-(v/c)**2)'])
        beta = float(BunchDic['beta=v/c'])
        RFTime = self.RFtime
        TimeP = float(BunchDic['SYNC_PART_TIME'])
        n_tuple = len(RFTime) - 1
        RFV = interp(TimeP, n_tuple, RFTime, self.RFV)
        phis = float(BunchDic['SYNC_PART_COORDS'][-1])*self.ZtoPhi
        h = 2
        GamaTrans = 4.89
        Yita = abs(1/GamaTrans/GamaTrans - 1/gama/gama)
        Ts = math.sqrt(2*math.pi*gama*M0/(h*RFV*Yita))*self.latticLenth/self.speed_of_light
        return TimeP,Ts
    
    def Plot_LostPart(self):
        Dump_Name = self.dump['name'].split(".")[0]
        Dump_Suffix = "bunch"
        bunchFileName = 'lost_'+str(self.dump['end'])+"_"+ Dump_Name+"_"+str(0.001)
        BunchFile = open(self.dump['folder']+'/'+bunchFileName+"."+ Dump_Suffix,'r')
        tempData = []
        x = []
        xp = []
        y = []
        yp = []
        z = []
        dE =[]
        Pos = []
        Time = []
        while True:
            templine = BunchFile.readline()
            if templine:
                if templine[0] == "%":
                    continue
                else: 
                    tempData = templine.split(" ")
            else:
                break
            if math.isnan(float(tempData[4])) or math.isnan(float(tempData[5])):
                print("Remove one line of invalid data from " + bunchFileName)
                continue
            x.append(float(tempData[0]))
            xp.append(float(tempData[1]))
            y.append(float(tempData[2]))
            yp.append(float(tempData[3]))
            z.append(float(tempData[4]))
            dE.append(float(tempData[5]))
            Pos.append(float(tempData[6]))
            Time.append(float(tempData[7])*1e3)
            tempData = []
        BunchFile.close()
        plt.figure()
        plt.title('Lost Particle ZphaseSpace',fontsize='large')
        plt.hist2d(z, dE, bins = 256, norm = LogNorm())
        plt.xlabel('z')
        plt.ylabel('dE')
        plt.savefig(self.picReposi+"LostPart_Zphasespace.png")
        plt.close()
        plt.figure()
        plt.title('Lost Particle ZDistribution',fontsize='large')
        plt.hist(z,bins = 256)
        plt.xlabel('z')
        plt.savefig(self.picReposi+"LostPart_ZDistribution.png")
        plt.close()      

        plt.figure()
        plt.title('Lost Particle Xphasespace',fontsize='large')
        plt.hist2d(x,xp,bins = 256 ,norm = LogNorm())
        plt.xlabel('x')
        plt.ylabel('xp')
        plt.savefig(self.picReposi+"LostPart_Xphasespace.png")
        plt.close()
        plt.figure()
        plt.title('Lost Particle XDistribution',fontsize='large')
        plt.hist(x,bins = 256)
        plt.xlabel('x')
        plt.savefig(self.picReposi+"LostPart_XDistribution.png")
        plt.close()

        plt.figure()
        plt.title('Lost Particle Yphasespace',fontsize='large')
        plt.hist2d(y,yp,bins = 256, norm = LogNorm())
        plt.xlabel('y')
        plt.ylabel('yp')
        plt.savefig(self.picReposi+"LostPart_Yphasespace.png")
        plt.close()
        plt.figure()
        plt.title('Lost Particle YDistribution',fontsize='large')
        plt.hist(y,bins = 256)
        plt.xlabel('y')
        plt.savefig(self.picReposi+"LostPart_YDistribution.png")
        plt.close()

        plt.figure()
        plt.title('Lost Particle Ring-Time Distribution',fontsize='large')
        plt.hist2d(Time, Pos, bins = 256, norm = LogNorm())
        plt.xlabel('Lost Time (ms)')
        plt.ylabel('Lost Position (m)')
        plt.savefig(self.picReposi+"LostPart_Ring-Time_Distribution.png")
        plt.close()
    ##########################################################
    # standardProcedure(self, IfCalBF = True, IfCalPF = False)
    # method : Automaticly plot phase space and write a table 
    #          of bunch information
    ##########################################################
    def standardProcedure(self, IfPlot = True, IfCalBF = True, IfCalPF = True, IfCalLonEmit = True, IfCalAlphaCoe = True):
        start = self.dump['start']
        end = self.dump['end']
        interval = self.dump['interval']
        if end == float("inf"):
            endturn = self.track['turn']
        else :
            endturn = end
        if start == 1:
        	startturn = interval
        else :
        	startturn = start
        turnList = list(range(startturn,endturn+interval,interval))
        Turn = start
        posList = self.dump['positions']
        pfList = []
        bfList = []
        LongiEmitList = []
        AlphaCoeList = []
        turnList = list(range(startturn, endturn+interval, interval))
        BunchInfo = {}
        TimeList = []
        for Turn in turnList:
            for pos in posList:
                if IfPlot:
                    self.plotPhaseSpace(Turn, pos)
                BunchInfo[str(Turn)+"turn_"+str(pos)] = self.__read_Bunch_Info(Turn, pos)
                TimeList.append(BunchInfo[str(Turn)+"turn_"+str(pos)]["SYNC_PART_TIME"]*1e3)
            
            if IfCalBF : 
                bfThisTurn = self.bf_cal(Turn, posList[0])
                bfList.append(bfThisTurn)
            if IfCalPF :
                pfThisTurn = self.pf_cal(Turn, posList[0])
                pfList.append(pfThisTurn)
            if IfCalLonEmit:
                LongiEmitThisTurn = self.calLonEmittance(Turn, posList[0])
                LongiEmitList.append(LongiEmitThisTurn)
            if IfCalAlphaCoe:
                if Turn == end:
                    continue
                AlphaCoeThisTurn = self.calAdiabaticCoefficient(Turn, posList[0])
                AlphaCoeList.append(AlphaCoeThisTurn)
            print("Turn "+str(Turn)+" has completed")
        
        if IfCalBF : 
            plt.figure()
            plt.title('Bunching Factor Changing By Turn',fontsize='large')
            plt.plot(TimeList, bfList)
            plt.xlabel('turn')
            plt.ylabel('buching factor')
            plt.savefig(self.picReposi+"BunchingFactor.png")
            plt.close()
            infobf = open(self.picReposi+"bunchfactorinfo.dat",'w')
            for turns in range(len(TimeList)):
                infobf.write(str(TimeList[turns])+'     '+str(bfList[turns]))
                infobf.write('\n')
            infobf.close()
        #CalPF in dual harmonic system only valid at the r !=0
        if IfCalPF :
            endIndex = turnList.index(4000)
            plt.figure()
            plt.title('Momentum Filling Factor Changing By Turn',fontsize='large')
            plt.plot(TimeList[0:endIndex], pfList[0:endIndex])
            plt.xlabel('turn')
            plt.ylabel('momentum filling factor')
            plt.savefig(self.picReposi+"MomentumFillingFactor.png")
            plt.close()
            
        if IfCalLonEmit:
            plt.figure()
            plt.title('Longitudinal Emittance Changing By Turn',fontsize='large')
            plt.plot(TimeList, LongiEmitList)
            plt.xlabel('turn')
            plt.ylabel('Longitudinal Emittance (eVs)')
            plt.savefig(self.picReposi+"LongitudinalEmittance.png")
            plt.close()
        if IfCalAlphaCoe:           
            plt.figure()
            plt.title('Adiabatic Coefficient Changing By Turn',fontsize='large')
            plt.plot(TimeList[:-6], AlphaCoeList[:-5])
            plt.xlabel('turn')
            plt.ylabel('Adiabatic Coefficient')
            plt.savefig(self.picReposi+"AdiabaticCoefficient.png")
            plt.close()

            
        #BunchInfo was writen in a data file
        fileBunchInfo =  open(self.picReposi+"BunchInfoTable.dat",'w')
        keywordsList = BunchInfo[str(start)+"turn_"+str(posList[0])].keys()
        fileBunchInfo.write("\t")
        for i in keywordsList:
            fileBunchInfo.write( i + "\t")
        fileBunchInfo.write('\n')
        for i in BunchInfo.keys():
            fileBunchInfo.write( i + '\t')
            for j in keywordsList:
                writenData = str(BunchInfo[i][j])
                fileBunchInfo.write(writenData+"\t")
            fileBunchInfo.write('\n')
        fileBunchInfo.close()
        

#############
#The following functions are used to calculate momentumFillingFactor(cal_pf)
def interp(x, n_tuple, x_tuple, y_tuple):
    """
    Linear interpolation: Given n-tuple + 1 points,
    x_tuple and y_tuple, routine finds y = y_tuple
    at x in x_tuple. Assumes x_tuple is increasing array.
    """
    if x <= x_tuple[0]:
        y = y_tuple[0]
        return y
    if x >= x_tuple[n_tuple]:
        y = y_tuple[n_tuple]
        return y
    dxp = x - x_tuple[0]
    for n in range(n_tuple):
        dxm = dxp
        dxp = x - x_tuple[n + 1]
        dxmp = dxm * dxp
        if dxmp <= 0:
            break
    y = (-dxp * y_tuple[n] + dxm * y_tuple[n + 1]) /\
        (dxm - dxp)
    return y
def FunctionOfPhibk2(phis,a2,phi2,phibk2):
    return np.array(np.sin(phibk2)-np.sin(phis)+a2*(-np.sin(2*(phibk2-phis)+phi2)+phibk2*np.sin(phi2)))
def solveTheFunction(phis,a2,phi2):
    init_guess = np.array([[math.pi-phis],[math.pi-phis],[math.pi-phis]])
    sol4_root = root(FunctionOfPhibk2,init_guess,args = (phis,a2,phi2))
    sol4_fsolve = fsolve(FunctionOfPhibk2,init_guess,args = (phis,a2,phi2))
    return sol4_fsolve[0]  

def calBinArea(dElist,i, nBins):
        if len(dElist[i]) >= 2:
            return max(dElist[i]) - min(dElist[i])
        elif i < nBins/2:
            return calBinArea(dElist, i+1, nBins)*0.1
        elif i > nBins/2:
            return calBinArea(dElist, i-1, nBins)*0.1
##############


    
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument("configfile", type=str, help="The config file!")
    args = parser.parse_args()
    
    with open(args.configfile) as f_in:
        Test = ToolPack(f_in)
        
    Test.standardProcedure(IfPlot = True, IfCalBF = True, IfCalPF = True, IfCalLonEmit = True, IfCalAlphaCoe = True)

    Test.Plot_LostPart()